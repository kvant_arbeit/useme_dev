# Облегчить себе разработку с питоном

### Как сделать питоновский пакет

http://klen.github.io/create-python-packages.html

https://the-hitchhikers-guide-to-packaging.readthedocs.io/en/latest/creation.html

Checklist here:

https://cookiecutter-pypackage.readthedocs.io/en/latest/tutorial.html#step-1-install-cookiecutter

#### + Setup_helpers
https://pypi.org/project/setup-helpers/
setup_helpers extend setuptools functionality.

Commands:

- `clean` - delete builds artefacts
- `upload` - upload builds artefacts to pypi
- `cv` -  check version, this command print warning to console if current version is already exists in pypi

Aliases:

- `build` - alias for `sdist bdist_wheel`
- `release` - alias for `build upload clean`


In this way if you want build package and deploy it to pypi you need run command

```bash
$ python setup.py release
```

Detailed example you can find in `setup.py` file, `repo` argument is a name of
section in [.pypirc](https://docs.python.org/3.6/distutils/packageindex.html#the-pypirc-file).
Instead of `repo` you can directly define `remote_url`, `remote_username` and `remote_password`.


Install:
```
$ pip install setup_helpers
```

## REST API with Flask

https://towardsdatascience.com/deploying-a-machine-learning-model-as-a-rest-api-4a03b865c166

Real example in my project


## Better Python

#### Виртуальное окружение

```
virtualenv -p python3 venv
```

```
# включить окружение
. venv/bin/activate
# деактивировать
deactivate
```

Установить зависимости в окружение
```
pip install -r requirements.txt
```

####  Code review

Установить прям внутрь редактора (в atom например встроить автоматически) или применять напрямую к конкретным файлам

PEP-8 formatter:
[black](https://github.com/ambv/black)

 ```
black python_file.py
 ```

Code audit:
[pylama](https://github.com/klen/pylama)

```
pylama
```

[**PyContracts**](https://pypi.org/project/PyContracts/)

https://censi.science/pub/research/201410-pycontracts/201410-pycontracts.pdf



## Tests

### TOX

[tox](https://tox.readthedocs.io/en/latest/)

Делаешь файл tox.ini.
Можно просто чтобы питон проверял, можно со стилями или контроль версий(cv - что пакет например такой уже существует, см. setup_helpers).

Например:
```
[tox]
envlist = py3, lint, cv
skipsdist = true

[testenv]
deps =
    -r{toxinidir}/requirements.txt

commands =
    pytest

[testenv:cv]
deps =
    -r{toxinidir}/requirements.txt

commands =
   python setup.py cv

[testenv:lint]
deps =
    -r{toxinidir}/requirements.txt

commands =
    pylint tests/
    pylint tests
```

Запустить :
```
tox
```

Сделать .pylintrc файл
https://github.com/PyCQA/pylint/blob/master/pylintrc

Например, содержимое файла может быть таким:
```
[TYPECHECK]
generated-members=id,objects

[MESSAGES CONTROL]
disable=missing-docstring,attribute-defined-outside-init,protected-access,too-few-public-methods,duplicate-code
```

### Unittests

[unittest](https://docs.python.org/3/library/unittest.html)

Лучше сразу папку сделать, если тестов много:
```
    tests/
        __init__.py
        some_test.py
```

The `setUp()` and `tearDown()` methods allow you to define instructions that will be executed before and after each test method.

Пример:
```

class TestTextParser(unittest.TestCase):
    """Test class for Token structure"""

    def setUp(self):
        self.token1 = Token()
        self.token1.lemma, self.token1.text = "привет", "Привет"
        self.token1.pos, self.token1.position = 1, 0
        self.token2 = Token()

    def test_token(self):
        """Check Token"""
        self.assertEqual(self.token3.key, "дело_1")
        self.assertEqual(self.token3.text, "дела")

    #  with self.assertRaises(TypeError):
    #     s.split(2)

    #  self.assertTrue('привет'.islower())
    #  self.assertFalse('привет'.isupper())

if __name__ == '__main__':
    unittest.main()
```


## Makefiles for python

https://krzysztofzuraw.com/blog/2016/makefiles-in-python-projects.html
