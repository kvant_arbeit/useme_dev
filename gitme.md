# Love git so much =)

Базовое:
```
$ git pull origin master
$ git log
...
$ git status
$ git add .
$ git commit -m "Message"
$ git push origin master
```

Работа с ветками:
```
$ git checkout -b <name_of_your_new_branch>
$ git branch
$ git checkout <branch>
$ git push origin <branch>
```

Удалить локально (-d / -D):
```
$ git branch -d <branch>
```

Удалить вообще:
```
$ git push origin :<branch>
```

Merge local:
- зайти в мастер
- смерджить все с дева
```
$ git merge dev_branch
```

Скипануть изменения:
```
$ git stash
```

Поставить и запушить тег
```
$ git tag -a 3.0.0 -m "Version 3.0.0"
$ git push --tags
```
