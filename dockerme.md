# Полюбить докер

## Основные команды

Посмотреть что есть:
```
docker ps -a
```

Сбилдить:
```
docker build --pull \
			--build-arg BUILD_CONFIG="$(CONFIG)" \
			-t $(REGISTRY)/$(PROJECT):latest \
			-t $(REGISTRY)/$(PROJECT):$(VERSION) .
```

Запустить контейнер:
```
docker run
```
Лучше с флагом -d , чтобы в бэкграунде запустился

Остановить:
```
docker stop
```


## Dockerfiles

Например для сайтов (пример для ангуляра с webpack):
```
FROM node:latest as build

WORKDIR /app

COPY package.json .

RUN npm i

COPY . .

ARG BUILD_CONFIG
RUN npm run build -- --prod --env $BUILD_CONFIG --build-optimizer

FROM nginx:1.13-alpine

MAINTAINER Alenush Fenogenova <alenush93@gmail.com>

COPY nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=build /app/dist/ /usr/share/nginx/html/
```

Просто через nginx:
```
FROM nginx:alpine

MAINTAINER Alenush Fenogenova <alenush93@gmail.com>

COPY nginx.conf /etc/nginx/conf.d/default.conf
COPY . /usr/share/nginx/html/
RUN rm /usr/share/nginx/html/nginx.conf

ENTRYPOINT nginx -g 'daemon off;'
```

Пример для питона:
```
FROM python:3.6

WORKDIR /src/

COPY ./requirements.txt ./requirements.txt
RUN pip install --no-cache-dir -r requirements.txt


# Download NLTK tokenization model
RUN python -m nltk.downloader -d /usr/local/share/nltk_data punkt

```
