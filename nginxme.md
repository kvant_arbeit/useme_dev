# Nginx

Установи (https://nginx.org/ru/)

#### Конфиги:

The default place of nginx.conf after installing:

On MAC:
```
/usr/local/etc/nginx/nginx.conf
```

on Linux:
```
/etc/nginx/nginx.conf
```

Слушает всегда по дефолту порт 8080
http://localhost:8080/

#### Как изменить порт:

We shall change it to 80. First stop the nginx server if it is running by:
```
sudo nginx -s stop
```

Поменять `listen 8080;` на `listen 80;`



Пример nginx.conf в проекте:

```
server {
  listen 80;
  keepalive_timeout 5;  
  server_tokens off;

  root /usr/share/nginx/html;

  charset utf-8;

  location / {
    try_files $uri $uri/ /index.html;
  }
}
```
